import json
import argparse
# import matplotlib.pyplot as plt
# from scipy.io.wavfile import write
from scipy.io import wavfile
import wave
import struct
import numpy as np
from wave_sig import generate # renamed from signal (circular import issue with matplotlib)

import wave

# SAMPLE_RATE = 8000
SAMPLE_RATE = 44100   # high quality WAV standard


def rescale_linear(array, new_min, new_max, old_max=None, old_min=None):
    # minimum, maximum = np.min(array), np.max(array)
    minimum = old_min if old_min is not None else np.min(array)
    maximum = old_max if old_max is not None else np.max(array)
    m = (new_max - new_min) / (maximum - minimum)
    b = new_min - m * minimum
    return m * array + b


def output(filename, x, y):
    if filename:
        # write pwl file
        with open(filename, 'w') as f:
            for i in range(len(x)):
                f.write(f"{x[i]}\t{y[i]}\n")
        # write wav file
        wav_filename = filename.split('.')[0] + '.wav'
        # y2 = (y - np.min(y)) / np.ptp(y) # [0,1]
        y2 = 2.0 * (y - np.min(y)) / np.ptp(y) - 1 # [-1,1]
        # y2 = rescale_linear(y, -1, 1, 5, -5) # this will scale volume as well
        # y2 = rescale_linear(y, -1, 1)
        wavfile.write(wav_filename, SAMPLE_RATE, y2)
        # with open(wav_filename, 'wb') as f:
        #     for b in y2:
        #         f.write(struct.pack('B', int(b * 255)))
    else:
        for i in range(len(x)):
            print(f"{x[i]}\t{y[i]}")


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-c',
        '--config',
        type=str,
        required=True,
        help='configuration file',
    )
    parser.add_argument(
        '-o',
        '--output',
        type=str,
        required=False,
        help='Write output to file.',
    )
    parser.add_argument(
        '-p',
        '--plot',
        required=False,
        action='store_true',
        help='Plot configuration file',
    )

    args = parser.parse_args()
    output_file = args.output
    config_file = args.config
    plot        = args.plot

    # read config file
    with open(config_file, 'r') as f:
        wave_params = json.load(f)
    # minor checks on config file
    if type(wave_params) is not list:
        raise Exception('Config File Invalid. Data must be a list.')

    x, y = generate(wave_params, sample_rate=SAMPLE_RATE)
    output(output_file, x, y)

    # # NOTE: remove this because I would have to install cython on pi and I don't want to
    # if plot:
    #     plt.plot(x, y)
    #     plt.title(config_file)
    #     plt.show()
