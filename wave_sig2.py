# import math
# import wave
# import struct

# # Audio will contain a long list of samples (i.e. floating point numbers describing the
# # waveform).  If you were working with a very long sound you'd want to stream this to
# # disk instead of buffering it all in memory list this.  But most sounds will fit in 
# # memory.
# SAMPLE_RATE = 44100.0


# def append_silence(duration_milliseconds=500):
#     num_samples = duration_milliseconds * (SAMPLE_RATE / 1000.0)
#     return [ 0.0 for x in range(int(num_samples)) ]

# def sinewave(freq=440.0, duration_milliseconds=500, volume=1.0):
#     num_samples = duration_milliseconds * (SAMPLE_RATE / 1000.0)
#     return [ (volume * math.sin(2 * math.pi * freq * ( x / SAMPLE_RATE ))) for x in range(int(num_samples)) ]

# def save_wav(file_name, audio):
#     # Open up a wav file
#     wav_file=wave.open(file_name,"w")
#     # wav params
#     nchannels = 1
#     sampwidth = 2
#     # 44100 is the industry standard sample rate - CD quality.  If you need to
#     # save on file size you can adjust it downwards. The stanard for low quality
#     # is 8000 or 8kHz.
#     nframes = len(audio)
#     comptype = "NONE"
#     compname = "not compressed"
#     wav_file.setparams((nchannels, sampwidth, SAMPLE_RATE, nframes, comptype, compname))
#     # WAV files here are using short, 16 bit, signed integers for the 
#     # sample size.  So we multiply the floating point data we have by 32767, the
#     # maximum value for a short integer.  NOTE: It is theortically possible to
#     # use the floating point -1.0 to 1.0 data directly in a WAV file but not
#     # obvious how to do that using the wave module in python.
#     for sample in audio:
#         wav_file.writeframes(struct.pack('h', int( sample * 32767.0 )))
#     wav_file.close()
#     return

# audio = []
# audio += sinewave(2175.0, 125, 0.125)
# audio += sinewave(2050.0, 40, 0.04)
# audio += sinewave(2175.0, 200, 0.200)

# save_wav('last.wav', audio)

import numpy as np
from scipy import signal as sg
from scipy.fftpack import fft
from scipy.io import wavfile

# DEF_SAMPLE_RATE = 5.e4
DEF_SAMPLE_RATE = 8000
# DEF_SAMPLE_RATE = 44100


def wav(filepath, scale=1, offset=0, dur=None, start=0, **kwargs):
    sample_freq, y = wavfile.read(filepath)
    data_type = y.dtype
    y = ((y / (2.0 ** 15)) * scale) + offset

    # calc duration and start offset
    duration = y.shape[0] / sample_freq
    dur = duration if dur is None else dur
    start_offset = int(start * sample_freq)
    # repeat or truncate for duration
    num_samples = int(dur * sample_freq)
    y1 = np.array([])
    while num_samples > 0:
        y1 = np.concatenate((y1, y[start_offset:start_offset+num_samples]))
        num_samples -= len(y)
    points = float(y1.shape[0])
    x = np.arange(0, points, 1)
    x = x / sample_freq
    return x, y1, sample_freq


def sin(freq, dur, samples=DEF_SAMPLE_RATE, **kwargs):
    time_interval = 1.0 / samples
    num_samples = int(dur / time_interval)
    x = np.linspace(0, dur, num_samples)
    y = np.sin(2 * np.pi * freq * x)
    return x, y, samples


def square(freq, dur, duty=0.5, samples=DEF_SAMPLE_RATE, **kwargs):
    time_interval = 1.0 / samples
    num_samples = int(dur / time_interval)
    x = np.linspace(0, dur, num_samples)
    y = sg.square(2 * np.pi * freq * x, duty=duty)
    return x, y, samples


def sawtooth(freq, dur, width=1, samples=DEF_SAMPLE_RATE, **kwargs):
    time_interval = 1.0 / samples
    num_samples = int(dur / time_interval)
    x = np.linspace(0, dur, num_samples)
    y = sg.sawtooth(2 * np.pi * freq * x, width=width)
    return x, y, samples


def generate(sigs):
    start = 0
    xs = []
    ys = []
    y2s = []

    for waves in sigs:
        x = None
        y = None
        y2 = None
        for wave in waves:
            w_type = wave.get('type', 'sin')

            if w_type == 'wav':
                xi, yi, sf = wav(**wave)
                print('yi max', np.max(yi))
                yi2 = yi
            else:
                # get wave type. default sin
                amp = wave.get('amp', None)
                vpp = wave.get('vpp', None)
                offset = wave.get('offset', 0)
                # if amp is not set use vpp
                if amp is None:
                    amp = vpp / 2
                    wave['amp'] = amp

                # generate wave
                if w_type == 'sin':
                    xi, yi, sf = sin(**wave)
                elif w_type == 'square':
                    xi, yi, sf = square(**wave)
                elif w_type == 'saw':
                    xi, yi, sf = sawtooth(**wave)
                yi2 = (yi * amp) + offset
                yi = (yi * amp / 5) + offset
            # incr wave start by prev wave end
            xi += start
            # set next start pos
            start = xi[-1]
            # concat to prev wave
            x = xi if x is None else np.concatenate((x, xi))
            y = yi if y is None else np.concatenate((y, yi))
            y2 = yi2 if y2 is None else np.concatenate((y2, yi2))

        xs.append(x)
        ys.append(y)
        y2s.append(y2)

    x = max(xs, key=len)

    max_y = max([len(y) for y in ys])
    _y = None
    for y in ys:
        padding = np.zeros(max_y)
        padding[:y.shape[0]] = y
        _y = padding if _y is None else _y + padding

    max_y2 = max([len(y) for y in y2s])
    _y2 = None
    for y in y2s:
        padding = np.zeros(max_y2)
        padding[:y.shape[0]] = y
        _y2 = padding if _y2 is None else _y2 + padding

    return x, _y, _y2

def scale(aval, amin, amax, bmin, bmax):
    return ( ( aval - amin ) / (amax - amin) ) * (bmax - bmin) + bmin

def example():
    s = [
        [
            { "type": "sin", "freq": 2175, "vpp": 2.190, "dur": 0.125 },
            { "type": "sin", "freq": 2050, "vpp": 0.692, "dur": 0.040 },
            { "type": "sin", "freq": 2175, "vpp": 0.109, "dur": 0.200 }
        ],
    ]
    fs = int(DEF_SAMPLE_RATE)
    samples = []
    for signals in s:
        # init sample array
        sample = np.array([])
        for signal in signals:
            # get parameters for signal
            vpp = signal['vpp']
            dur = signal['dur']
            frq = signal['freq']
            typ = signal.get('type', 'sin')
            width = signal.get('width', 1)
            duty = signal.get('duty', 0.5)
            # calculate wav volume from voltage peak to peak 0-5
            volume = scale(vpp, 0, 5, 0, 1)
            # generate signal
            val = 2 * np.pi * np.arange(fs * dur) * frq / fs
            if typ == 'saw':
                _sig = (sg.sawtooth(val, width=width) * volume)
            elif typ == 'square':
                _sig = (sg.square(val, duty=duty) * volume)
            else: # default sin
                _sig = (np.sin(val) * volume)
            # convert to wav acceptable format
            _sig = _sig.astype(np.float32)
            sample = np.concatenate((sample, _sig))
        # append sample
        samples.append(sample)
    # combine
    max_dur = max([ sum([ sig['dur'] for sig in sigs ]) for sigs in s ])
    max_len = max([ len(s) for s in samples ])
    sample = np.zeros(max_len)
    for s in samples:
        sample[0:len(s)] += s
    wavfile.write('last.wav', fs, sample)


def join(A, B):
    Cjump = np.concatenate([A[:-10], B[10:]])
    AP = np.unwrap(np.angle(sg.hilbert(A)))
    BP = np.unwrap(np.angle(sg.hilbert(B)))
    CP = np.concatenate([AP[:-10], BP[10:] - BP[10] + AP[-10]])
    C = np.cos(CP)
    return C

if __name__ == '__main__':
    s = [
        [
            { "type": "sin", "freq": 2175, "vpp": 2.190, "dur": 0.015 },
        ]
    ]
    fs = int(DEF_SAMPLE_RATE)
    samples = []
    for signals in s:
        # init sample array
        sample = np.array([])
        for signal in signals:
            # get parameters for signal
            vpp = signal['vpp']
            dur = signal['dur']
            frq = signal['freq']
            typ = signal.get('type', 'sin')
            width = signal.get('width', 1)
            duty = signal.get('duty', 0.5)
            # calculate wav volume from voltage peak to peak 0-5
            volume = scale(vpp, 0, 5, 0, 1)
            # generate signal
            val = 2 * np.pi * np.arange(fs * dur) * frq / fs
            if typ == 'saw':
                _sig = (sg.sawtooth(val, width=width) * volume)
            elif typ == 'square':
                _sig = (sg.square(val, duty=duty) * volume)
            else: # default sin
                _sig = (np.sin(val) * volume)
            # convert to wav acceptable format
            # _sig = _sig.astype(np.float32)
            _sig = _sig.astype(np.float32)
            # sample = join(sample, _sig) if len(sample) > 0 else np.concatenate((sample, _sig))
            sample = np.concatenate((sample, _sig))
        # append sample
        samples.append(sample)

    # combine
    max_dur = max([ sum([ sig['dur'] for sig in sigs ]) for sigs in s ])
    max_len = max([ len(s) for s in samples ])
    sample = np.zeros(max_len)
    for s in samples:
        sample[0:len(s)] += s


    sample = np.asarray(np.clip(sample * 30000, -32768, 32767), dtype=np.int16)
    wavfile.write('last.wav', fs, sample)

    x = np.linspace(0, max_dur, len(sample))
    with open('test.txt', 'w') as f:
        for i in range(len(sample)):
            f.write(f"{x[i]}\t{sample[i]}\n")