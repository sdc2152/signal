VERSION: 1.1
    config files can now combine signals

usage: python main.py -c CONFIG [-o OUTPUT] [-p]

required arguments
  -c CONFIG, --config CONFIG
                        configuration file
                        
optional arguments:
  -o OUTPUT, --output OUTPUT
                        Write output to file.

  -p, --plot            Plot configuration file


COMMON VALUES
-26 db = 0.109 vpp
-10 db = 0.692 vpp

15 ms


best example config files
    - ea_test_new.json
    - ea_test_voice.json

    other json files might have old format

config file
    json file
    contains a single list of waveforms
    example:
        High Low Test:
            [
                [
                    { "type": "sin", "freq": 2175, "amp": 1.0950, "dur": 0.200, "offset": 2.5 },
                    { "type": "sin", "freq": 2175, "amp": 0.0950, "dur": 0.200, "offset": 2.5 }
                ]
            ]
        EIA:
            [
                [
                    { "type": "sin", "freq": 2175, "vpp": 2.190, "dur": 0.125, "offset": 2.5 },
                    { "type": "sin", "freq": 2050, "vpp": 0.692, "dur": 0.040, "offset": 2.5 },
                    { "type": "sin", "freq": 2175, "vpp": 0.109, "dur": 0.200, "offset": 2.5 }
                ]
            ]
        Square Example
            [
                [
                    { "type": "square", "freq": 2175, "vpp": 2.190, "dur": 0.125, "offset": 2.5, "duty": 0.5 },
                    { "type": "square", "freq": 2050, "vpp": 0.692, "dur": 0.040, "offset": 2.5, "duty": 0.8 },
                    { "type": "square", "freq": 2175, "vpp": 0.109, "dur": 0.200, "offset": 2.5 }
                ]
            ]
        Saw Example:
            [
                [
                    { "type": "saw", "freq": 2175, "vpp": 2.190, "dur": 0.125, "offset": 2.5, "width": 0.5 },
                    { "type": "saw", "freq": 2050, "vpp": 0.692, "dur": 0.040, "offset": 2.5, "width": 0.8 },
                    { "type": "saw", "freq": 2175, "vpp": 0.109, "dur": 0.200, "offset": 2.5 }
                ]
            ]
        WAV file example:
            [
                [
                    { "type": "wav", "filepath": "audio/harvard_sentence1.wav", "dur": 2.190, "scale": 2, "offset": 2.5 },
                    { "type": "wav", "filepath": "audio/harvard_sentence2.wav", "dur": 2.190, "scale": 3, "offset": 2.5 },
                    { "type": "wav", "filepath": "audio/harvard_sentence3.wav", "dur": 2.190, "scale": 4, "offset": 2.5 }
                ]
            ]

    combining signals
        each array in top level array will be combined
        the below example has 4 signals all of with a duration of 1 second.
        they will be combined to create a single 1 second signal
            [
                [
                    { "type": "wav", "filepath": "audio/harvard_sentence1.wav", "start": 2, "dur": 1, "scale": 2, "offset": 0 }
                ],
                [
                    { "type": "wav", "filepath": "audio/harvard_sentence2.wav", "start": 2, "dur": 1, "scale": 3, "offset": 0 }
                ],
                [
                    { "type": "wav", "filepath": "audio/harvard_sentence3.wav", "start": 2, "dur": 1, "scale": 4, "offset": 0 }
                ],
                [
                    { "type": "sin", "freq": 2175, "vpp": 2.190, "dur": 1, "offset": 2.5 }
                ]
            ]

        each array in top level array will be combined
        the below example has 4 signals all of with a duration of 1 second.
        because they are in the same array they will be concatenated to create a 4 second signal
            [
                [
                    { "type": "wav", "filepath": "audio/harvard_sentence1.wav", "start": 2, "dur": 1, "scale": 2, "offset": 0 },
                    { "type": "wav", "filepath": "audio/harvard_sentence2.wav", "start": 2, "dur": 1, "scale": 3, "offset": 0 },
                    { "type": "wav", "filepath": "audio/harvard_sentence3.wav", "start": 2, "dur": 1, "scale": 4, "offset": 0 },
                    { "type": "sin", "freq": 2175, "vpp": 2.190, "dur": 1, "offset": 2.5 }
                ]

            ]

    waveform params:
        type    : [ 'sin' , 'square', 'saw' , 'wav' ]. Default is 'sin'.
        freq    : frequency in Hz
        amp     : amplitude
        vpp     : (optional) Voltage peak to peak. If amp and vpp are set amp will be used.
        dur     : duration in seconds
        offset  : y offset
        duty    : (square only) Duty cycle. Default is 0.5 (50% duty cycle)
        width   : (saw only) Width of the rising ramp as a proportion of the total cycle. Default is 1.
        scale   : (wav only) min max value 2 will be between 2 and -2, 1 will be between 1 and -1
        filepath: (wav only) filepath to wav file
        start   : (wav only) beginning offset into wav file
