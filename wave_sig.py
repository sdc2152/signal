import numpy as np
from scipy import signal as sg
from scipy.fftpack import fft
from scipy.io import wavfile


# DEF_SAMPLE_RATE = 44100   # high quality WAV standard
DEF_SAMPLE_RATE = 8000      # low quality WAV standard


def wav(filepath, scale=1, offset=0, dur=None, start=0, **kwargs):
    sample_freq, y = wavfile.read(filepath)
    data_type = y.dtype
    y = ((y / (2.0 ** 15)) * scale) + offset
    # calc duration and start offset
    duration = y.shape[0] / sample_freq
    dur = duration if dur is None else dur
    start_offset = int(start * sample_freq)
    # repeat or truncate for duration
    num_samples = int(dur * sample_freq)
    y1 = np.array([])
    while num_samples > 0:
        y1 = np.concatenate((y1, y[start_offset:start_offset+num_samples]))
        num_samples -= len(y)
    points = float(y1.shape[0])
    x = np.arange(0, points, 1)
    x = x / sample_freq
    return x, y1, sample_freq


def sin(freq, amp, dur, samples=DEF_SAMPLE_RATE, offset=0, **kwargs):
    time_interval = 1.0 / samples
    num_samples = int(dur / time_interval)
    x = np.linspace(0, dur, num_samples)
    y = (np.sin(2 * np.pi * freq * x) * amp) + offset
    return x, y, samples


def square(freq, amp, dur, duty=0.5, samples=DEF_SAMPLE_RATE, offset=0, **kwargs):
    time_interval = 1.0 / samples
    num_samples = int(dur / time_interval)
    x = np.linspace(0, dur, num_samples)
    y = (sg.square(2 * np.pi * freq * x, duty=duty) * amp) + offset
    return x, y, samples


def sawtooth(freq, amp, dur, width=1, samples=DEF_SAMPLE_RATE, offset=0, **kwargs):
    time_interval = 1.0 / samples
    num_samples = int(dur / time_interval)
    x = np.linspace(0, dur, num_samples)
    y = (sg.sawtooth(2 * np.pi * freq * x, width=width) * amp) + offset
    return x, y, samples


def generate(sigs, sample_rate=DEF_SAMPLE_RATE):
    start = 0
    xs = []
    ys = []
    for waves in sigs:
        x = None
        y = None
        for wave in waves:
            w_type = wave.get('type', 'sin')
            if w_type == 'wav':
                xi, yi, sf = wav(**wave)
            else:
                # get wave type. default sin
                amp = wave.get('amp', None)
                vpp = wave.get('vpp', None)
                # if amp is not set use vpp
                if amp is None:
                    wave['amp'] = vpp / 2
                # generate wave
                if w_type == 'sin':
                    xi, yi, sf = sin(samples=sample_rate, **wave)
                elif w_type == 'square':
                    xi, yi, sf = square(samples=sample_rate, **wave)
                elif w_type == 'saw':
                    xi, yi, sf = sawtooth(samples=sample_rate, **wave)
            # incr wave start by prev wave end
            xi += start
            # set next start pos
            start = xi[-1]
            # concat to prev wave
            x = xi if x is None else np.concatenate((x, xi))
            y = yi if y is None else np.concatenate((y, yi))
        xs.append(x)
        ys.append(y)
    x = max(xs, key=len)
    max_y = max([len(y) for y in ys])

    _y = None
    for y in ys:
        padding = np.zeros(max_y)
        padding[:y.shape[0]] = y
        _y = padding if _y is None else _y + padding

    return x, _y